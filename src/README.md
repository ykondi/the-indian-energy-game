Django Based Prototype
======================

This directory contains the prototype for the Indian Energy Game. Made using
Django. Testing done primarily using Chrome.

The manage.py file is in the energy_game folder.

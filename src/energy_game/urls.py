from django.conf.urls import patterns, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.views.generic import TemplateView

urlpatterns = patterns('',
	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	# Uncomment the next line to enable the admin:
	# url(r'^admin/', include(admin.site.urls)),

	# Global
	url(r'^$', 'interface.views.login_page',{}, name="home"),

	# Facilitator URLs
	# DB control
	url(r'^newuser', 'interface.facilitator.manage_dbase.add_user'),
	url(r'^addconstraints', 'interface.facilitator.manage_dbase.add_constraints'),
	# Sessions
	url(r'^sessions$', 'interface.facilitator.session.sessions_list'),
    	url(r'^controlsession/(\d+)$', 'interface.facilitator.session.session_control'),
	url(r'^newsession$', 'interface.facilitator.session.new_session'),
	url(r'^userlist$', 'interface.facilitator.session.user_ids'),
	url(r'^budget/(\d+)$', 'interface.facilitator.session.budget_control'),
	# State
	url(r'^nextstate', 'interface.facilitator.state.next_state'),
	# Messages
	url(r'^newmessage$', 'interface.facilitator.messages.quick_note_taker'),
	url(r'^newmessage/(\d+)$', 'interface.facilitator.messages.quick_note_taker'),
	url(r'^newmessage/messagebank$', 'interface.facilitator.messages.message_bank'),

	# Player URLs
	# Budget
    	url(r'^currentval$', 'interface.player.budget.current_budget'),
	# Main page
	url(r'^mainpage$', 'interface.player.main.main_page'),
	url(r'^mainpage/statecheck$', 'interface.player.main.state_response'),
	# Notes
	url(r'^notetaker', 'interface.player.notes.render_notes'),
	url(r'^newnote$', 'interface.player.notes.quick_note_taker'),
	url(r'^notedisplay$', 'interface.player.notes.note_display'),
	# Session
	url(r'^choosesession$', 'interface.player.session.choose_session'),
	# Informative pages
    	url(r'^instructions$', 'interface.player.information.instructions'),
	url(r'^numbers$', 'interface.player.information.energy_numbers'),
	url(r'^power$', 'interface.player.information.pd'),
	# Not requested by users; used for recording data:
	url(r'^static$', 'django.views.static.serve'),
	url(r'^test$', 'interface.alt.alt.test'),
)

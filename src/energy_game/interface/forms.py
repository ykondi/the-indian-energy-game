from django import forms
from django.contrib.auth.models import User
from bootstrap_toolkit.widgets import BootstrapDateInput, BootstrapTextInput, BootstrapUneditableInput
class TestForm(forms.Form):
    prepended = []
    types = ['A', 'B', 'C']
    for i in types:
    	prepended.append(forms.CharField(
        	max_length=8,
        	widget=BootstrapTextInput(prepend='Type '+i),
    	))
    Coal1 = prepended[0]
    Coal2 = prepended[1]
    Coal3 = prepended[2] 

    def clean(self):
        cleaned_data = super(TestForm, self).clean()
        raise forms.ValidationError("This error was added to show the non field errors styling.")
        return cleaned_data

class QuickNote(forms.Form):
    title = forms.CharField(max_length=100,)
    content = forms.CharField(max_length=2000,)

class TestModelForm(forms.ModelForm):
    class Meta:
        model = User


class TestInlineForm(forms.Form):
    query = forms.CharField(required=False, label="")
    vegetable = forms.ChoiceField(
        choices=(
            ("broccoli", "Broccoli"),
            ("carrots", "Carrots"),
            ("turnips", "Turnips"),
        ),
    )
    active = forms.ChoiceField(widget=forms.RadioSelect, label="", choices=(
        ('all', 'all'),
        ('active', 'active'),
        ('inactive', 'inactive')
        ), initial='all')
    mine = forms.BooleanField(required=False, label='Mine only', initial=False)


class WidgetsForm(forms.Form):
    date = forms.DateField(widget=BootstrapDateInput)


class FormSetInlineForm(forms.Form):
    UserName = forms.CharField()
    Password = forms.CharField(widget = forms.PasswordInput)


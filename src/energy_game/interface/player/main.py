'''
	Container for the player's main page
	* main_page	 : Player's main page
	* state_response : Response to ajax request checking FYP activity
'''

from import_elements import *
from budget import AmountObject, BudgetFromObject

def main_page(request):
	# Check if user is signed in:
	if request.session.get('userid') == None or request.session.get('userid') == 'facilitator':
		return HttpResponse('Access Denied. You must be logged in to view this page.')

	# Check what state the session is in
	theSession = SessionState.objects.get(session_id = request.session.get('session_id')).session_state

	if request.method == 'POST':
		# Check if FYP is currently active before allowing values to be submitted:
		if theSession == 1:
			return HttpResponse('The 12th Five Year Plan has ended. Please refresh your screen to view the summary.')
		elif theSession == 3:
			return HttpResponse('The game has ended. Please refresh your screen to view the summary.')
		# Record current budget configuration:
		amount_id = []
		r = redis.StrictRedis(host='localhost', port=6379, db=0)
		for i in Resources.objects.filter(role_id=request.session['role']):
			for j in range(0, 3):
				amount_id.append(i.resource_id*3 + j)
		
		tempBudget = r.lrange(request.session['team'].team_id, 0, r.llen(request.session['team'].team_id))
		# Debug log
		dbg = open('/tmp/dbg', 'w')
		if len(tempBudget) == 0:
			dbg.write("Not going to update\n")
			try:
				tempBudget = BudgetFromObject( Amount.objects.filter(team_id = request.session['team'].team_id)[0] )
			except IndexError:
				tempBudget = [0 for i in range(0, 3*len(Resources.objects.all()))]
		else:
			r.delete(request.session['team'].team_id)
		for amount in amount_id:
			tempBudget[amount] = int(request.POST.get(str(amount)))
		
		# Update Redis cache:
		for i in range(len(tempBudget)-1, -1, -1):
			r.lpush(request.session['team'].team_id, tempBudget[i])

		# Update database log:
		theAmount = AmountObject(request.session['team'].team_id, tempBudget)
		theAmount.save()
		# Return success
		return HttpResponse('S')

	# Set names of ministry and get resources
	roles = ['Ministry of Power', 'Ministry of New and Renewable Energy', 'Department of Atomic Energy']
	ministry = roles[request.session.get('role')]
	plans = [12, 12, 13, 13]

	if theSession % 2 == 1:
		# Currently not in a playable FYP
		fyp = plans[theSession]
		role_messages = EndMessage.objects.filter(role_id = request.session.get('role'), team_id = request.session.get('team'))
		role_messages = role_messages[len(role_messages)-1]
		public_messages = EndMessage.objects.filter(role_id = -1, team_id = request.session.get('team'))
		public_messages = public_messages[len(public_messages)-1]
		return render(request, 'summary.html', {'role_messages':role_messages, 'public_messages':public_messages,
					'ministry':ministry, 'fyp':fyp,
					'userName': User.objects.get(user_id=request.session.get('userid')).user_name})

	fyp = plans[theSession]
	temp_resources = Resources.objects.filter(role_id=request.session.get('role'))
	# Initializations
	resources = [x.resource_name for x in temp_resources]
	cost_of_installation = []
	cost_of_generation = []
	carbon_emitted = []
	temp_energy_numbers = []
	energy_numbers = []
	relevantBudget = []
	role_id = request.session.get('role')

	# List to mark the starting indices of different roles' resources
	role_pointer = [0, 9, 15, 17]

	for i in range(0,(3 - role_id)):
		for j in range(0,3):
			temp_energy_numbers = ResourcePrice.objects.filter(resource_no = (role_pointer[role_id] + i + j) ,  team_id = request.session['team'].team_id)
			for t in temp_energy_numbers:
				energy_numbers.append(t)
	carbon_emitted = [x.carbon_emitted for x in energy_numbers]
	cost_of_generation = [x.cost_of_generation for x in energy_numbers]
	cost_of_installation = [x.cost_of_installation for x in energy_numbers]	
	# Pass budget values:
	r = redis.StrictRedis(host='localhost', port=6379, db=0)
	theBudget = [int(i) for i in r.lrange(request.session['team'].team_id, 0, r.llen(request.session['team'].team_id))]
	relevantBudgetValues = theBudget[role_pointer[role_id] : role_pointer[role_id+1]]
	counter = 0
	for i in temp_resources:
		res_val_list = []
		for j in range(counter, counter + 3):
			res_val_list.append(relevantBudgetValues[j])
		relevantBudget.append(res_val_list)
		counter = counter + 3
	# Combine resource names and amounts
	resource_structure = []
	for i in range(0, len(temp_resources)):
		resource_structure.append((temp_resources[i], relevantBudget[i]))

	return render_to_response('mainpage.html', RequestContext(request, {
		'ministry': ministry,
		'resources2': resources,
		'resources': resource_structure,
		'carbonemissions':carbon_emitted,
		'costofgeneration':cost_of_generation,
		'costofinstallation':cost_of_installation,
		'userName': User.objects.get(user_id=request.session.get('userid')).user_name,
		'theBudget': theBudget,
		'fyp':fyp,
    }))

def state_response(request):
	session_id = request.session['session_id']
	state = SessionState.objects.get(session_id = session_id).session_state
	if state % 2 == 0:
		# FYP still active. Return sucess.
		return HttpResponse('S')
	return HttpResponse('N')

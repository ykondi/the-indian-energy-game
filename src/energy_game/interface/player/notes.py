'''
	Views to deal with notes
	* quick_note_taker	: Enter and add a note
	* render_notes		: Display a list of relevant notes
	* note_display		: Display contents of a note
'''

from import_elements import *
from interface.forms import QuickNote

def quick_note_taker(request):
	c = {'role': request.session['role']}
	
	c.update(csrf(request))
	if request.method == 'POST':
		form = QuickNote(request.POST)
		if form.is_valid():
			newNote = Notes(note_title=request.POST['title'], note_content=request.POST['content'], user_id=request.session.get('userid'), role_id=request.session.get('role'), team=request.session.get('team'))
			newNote.save()
			return HttpResponse('Note Added.\nYou may close this window now.')
		else:
			return HttpResponse('Note failed to be recorded.')
	return render_to_response('newnote.html', c)

def render_notes(request):
	notes = Notes.objects.filter( Q(role_id = request.session.get('role'), team_id = request.session.get('team')) | Q(role_id = -1, team_id = request.session.get('team')))
	if request.method == 'POST':
		 if request.POST.get('Search') != '':
			validNotes = []
			keywords = request.POST.get('Search').split()
			for note in notes:
				for word in keywords:
					if (word in note.note_title) or (word in note.note_content):
						validNotes.append(note)
						break
			notes = validNotes
				
	roles = ['Ministry of Power', 'Ministry of New and Renewable Energy', 'Department of Atomic Energy']
	ministry = roles[request.session.get('role')]

	return render( request, 'notetaker.html', {'notes':notes,
							'ministry': ministry})

def note_display(request):
	noteName = request.GET.get('note')
	theNote = Notes.objects.get( note_id = noteName )
	return render_to_response( 'notedisplay.html', {'note':theNote})


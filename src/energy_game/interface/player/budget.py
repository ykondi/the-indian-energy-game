'''
	Budget-related utilities
	* AmountObject		: Returns an Amount object given a team and budget
	* BudgetFromObject	: Returns a list of the budget, given an amount obj

	Views
	* current_budget	: Return team energy mixture
'''

from import_elements import *

def AmountObject(team, theTeamB):
	amountObject = Amount(team_id = team)
	amountObject.number_1 = theTeamB[0]
	amountObject.number_2 = theTeamB[1]
	amountObject.number_3 = theTeamB[2]
	amountObject.number_4 = theTeamB[3]
	amountObject.number_5 = theTeamB[4]
	amountObject.number_6 = theTeamB[5]
	amountObject.number_7 = theTeamB[6]
	amountObject.number_8 = theTeamB[7]
	amountObject.number_9 = theTeamB[8]
	amountObject.number_10 = theTeamB[9]
	amountObject.number_11 = theTeamB[10]
	amountObject.number_12 = theTeamB[11]
	amountObject.number_13 = theTeamB[12]
	amountObject.number_14 = theTeamB[13]
	amountObject.number_15 = theTeamB[14]
	amountObject.number_16 = theTeamB[15]
	amountObject.number_17 = theTeamB[16]
	amountObject.number_18 = theTeamB[17]
	return amountObject

def BudgetFromObject(amountObject):
	budget = []
	budget.append(amountObject.number_1)
	budget.append(amountObject.number_2)
	budget.append(amountObject.number_3)
	budget.append(amountObject.number_4)
	budget.append(amountObject.number_5)
	budget.append(amountObject.number_6)
	budget.append(amountObject.number_7)
	budget.append(amountObject.number_8)
	budget.append(amountObject.number_9)
	budget.append(amountObject.number_10)
	budget.append(amountObject.number_11)
	budget.append(amountObject.number_12)
	budget.append(amountObject.number_13)
	budget.append(amountObject.number_14)
	budget.append(amountObject.number_15)
	budget.append(amountObject.number_16)
	budget.append(amountObject.number_17)
	budget.append(amountObject.number_18)
	return budget


# Return the current energy mixture of the entire team:
def current_budget(request):
	data = {}
	data['resources'] = Resources.objects.all()
	# Rearrange the data in the format [ [All Type A] [All Type B] [All Type C] ] and pass it to template
	temp = [[], [], []]
	r = redis.StrictRedis(host='localhost', port=6379, db=0)
	theBudget = [int(i) for i in r.lrange(request.session['team'].team_id, 0, r.llen(request.session['team'].team_id))]
	tempBudget = r.lrange(request.session['team'].team_id, 0, r.llen(request.session['team'].team_id))
	for i in range(0, len(theBudget), 3):
		for j in range(0, len(temp)):
			temp[j].append(theBudget[i+j])
	data['budgetConfig'] = temp
	return render(request, 'currentbudget.html', data)


'''
	Manage which session to be part of
	* UpdateSession
	* choose_session
'''

from import_elements import *


def UpdateSession(request, sessionID):
	request.session['session'] = int(sessionID)
	request.session['session_id'] = int(sessionID)
	for userTeam in UserTeam.objects.filter(user_id=request.session['userid']):
		if Team.objects.get( team_id=userTeam.team_id ).session_id == request.session['session']:
			request.session['team'] = Team.objects.get( team_id=userTeam.team_id )
			break
	request.session['role'] = UserTeam.objects.get(team_id=request.session['team'], user_id=request.session['userid']).role_id
	
	# Update latest budget:
	try:
		amounts = Amount.objects.filter(team_id = request.session['team'])[0]
	except IndexError:
		amounts = [0 for i in range(0, 3*len(Resources.objects.all()))]
	request.session['budget'] = amounts
	return request.session


def choose_session(request):
	returnDict = {}
	returnDict.update(csrf(request))
	if request.session.get('userid') == None:
		return HttpResponse('Access Denied. You must be logged in to view this page.')
	if request.method == 'GET':
		RelevantSessions = [ Session.objects.get(session_id=Team.objects.get(team_id=ut.team_id).session_id) for ut in UserTeam.objects.filter(user_id=request.session['userid']) ]
		if len(RelevantSessions) == 1:
			request.session = UpdateSession( request, RelevantSessions[0].session_id )
			return HttpResponseRedirect('/instructions')
		else:
			returnDict['sessions'] = RelevantSessions
			return render(request, 'choose_session.html', returnDict)
	elif request.method == 'POST':
		try:
			request.session['session'] = int(request.POST.get('session'))
		except request.POST.DoesNotExist:	
			RelevantSessions = [ Session.objects.get(session_id=Team.objects.get(team_id=ut.team_id).session_id) for ut in UserTeam.objects.filter(user_id=request.session['userid']) ]
			returnDict['sessions'] = RelevantSessions
			return render(request, 'choose_session.html', returnDict)
		request.session = UpdateSession(request, request.POST.get('session'))
		return HttpResponseRedirect('/instructions')

	return HttpResponse('Invalid Request.')

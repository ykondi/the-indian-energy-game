'''
	Containter for informative pages
	* instructions
	* energy numbers
'''

from import_elements import *

def instructions(request):
	if request.session.get('userid') == None:
		return HttpResponse('Access Denied. You must be logged in to view this page.')
	role = Role.objects.get(role_id = request.session['role'])
	ministry = ['mop', 'mnre', 'dae']
	return render(request, 'instructions_'+ministry[request.session.get('role')]+'.html')

def energy_numbers(request):
	temp_resources = Resources.objects.filter(role_id=request.session.get('role'))
	resources = [x.resource_name for x in temp_resources]
	cost_of_installation = []
	cost_of_generation = []
	carbon_emitted = []
	role_pointer = [0,9,15]
	temp_energy_numbers = []
	energy_numbers = []
	role_id = request.session.get('role')
	for i in range(0,(3 - role_id)):
		for j in range(0,3):
			temp_energy_numbers = ResourcePrice.objects.filter(resource_no = (role_pointer[role_id] + i + j + 1), team_id = request.session['team'].team_id)
			for t in temp_energy_numbers:
				energy_numbers.append(t)
	resourceList = []
	for i in range(0, len(resources)):
		resourceList.append(resources[i] + ' Type A')
		resourceList.append(resources[i] + ' Type B')
		resourceList.append(resources[i] + ' Type C')
	output = zip(resourceList,energy_numbers)
	return render_to_response('energy_numbers.html', RequestContext(request, {
		'output':output
    }))

def pd(request):
    return render(request, 'pd.html')

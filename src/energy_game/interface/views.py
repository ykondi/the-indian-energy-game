from import_elements import *

# Login Page:

def login_page(request):
	c = {}
	c.update(csrf(request))
	if request.method == 'GET':
		request.session['userid'] = None
		theTime = time.localtime()
		request.session['sessionID'] = str(theTime[2])+'-'+str(theTime[1])+'-'+str(theTime[0])+'-'+str(theTime[3])+':'+str(theTime[4])
		c['formset'] = formset_factory(FormSetInlineForm)
		return render_to_response('login.html', c)
	
	elif request.method == 'POST':
		username = request.POST['form-0-UserName']
		password = request.POST['form-0-Password']
		user = User.objects.filter( user_name = username)
		if len(user) != 0:
			user = user[0]
			if password == user.password:
				request.session['userid'] = user.user_id
				if username == 'facilitator':
					return HttpResponseRedirect('sessions')
				return HttpResponseRedirect('choosesession')
		c['formset'] = formset_factory(FormSetInlineForm)
		return render_to_response('login_failed.html', c)
	
	return HttpResponse('Invalid Request')






'''
	Miscellaneous utils.

	* Authenticate_Facilitator : Function to check if facl. is logged in
'''

from interface.models import User

def Authenticate_Facilitator(request):
	try:
		if User.objects.get(user_id = request.session.get('userid')).user_name != 'facilitator':
                	return False
	except User.DoesNotExist:
                return False
	return True

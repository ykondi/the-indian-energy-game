'''
	Interfaces to insert elements into the database

	* add_user	  : Add a user+pwd to the database
	* add_constraints : Add constraints to the db

'''

from import_elements import *
from misc import Authenticate_Facilitator

# View to add new user
def add_user(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	if request.method == 'POST':
		newUser = User(user_id = len(User.objects.all()), user_name = request.POST['name'],
				password = request.POST['password'])
		newUser.save()
		return HttpResponse('Login created for ' + request.POST['name'])
	returnDict = {}
	returnDict.update(csrf(request))
	return render(request, 'newuser.html', returnDict)


def add_constraints(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	if request.method == 'POST':
		for i in range(0,18):
					ll = int(request.POST.get(str(i)))
					ul = int(request.POST.get(str(i) + 'u'))
					temp_cons = Constraints.objects.get( constraint_id = len(Constraint.objects.all()))
					temp_cons.session_id = request.session['session'].session_id
					temp_cons.lower_limit = ll
					temp_cons.upper_limit = ul
					temp_cons.save()
	types = ['Type A', 'Type B', 'Type C']
	resources = []
	tempresources = Resources.objects.all()
	for resource in tempresources:
		for t in types:
			resources.append(resource.resource_name  + " " + t)
	return render(request, 'addconstraints.html',{'resources':resources})

def add_energy_numbers(request):
	if not Authenticate_Facilitator(request):
	 return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
        if request.method == 'POST':
                for i in range(0,18):
                                        cost_of_installation = int(request.POST.get("cost_install"))
                                        cost_of_generation = int(request.POST.get("cost_gener"))
                                        carbon_emitted = int(request.POST.get("carbon"))
                                        temp_energy_number = ResourcePrice.objects.get( id = len(ResourcePrice.objects.all()))
                                        temp_energy_number.team_id = request.session['team'].team_id
					temp_energy_number.resource_no_id = i
                                        temp_energy_number.cost_of_installation = cost_of_installation
                                        temp_energy_number.cost_of_generation  = cost_of_generation
                                        temp_energy_number.carbon_emitted = carbon_emitted
                                        temp_energy_number.save()
	else:	
                for i in range(0,18):
			temp_energy_number = ResourcePrice.objects.get( id = len(ResourcePrice.objects.all()))
			temp_standard_number = StandardResourcePrices.objects.filter( resource_no = i)
			temp_energy_number.team_id = request.session['team'].team_id
			temp_energy_number.resource_no_id = i
                        temp_energy_number.cost_of_installation = temp_standard_number.cost_of_installation
                        temp_energy_number.cost_of_generation  = temp_standard_number.cost_of_generation
                        temp_energy_number.carbon_emitted = temp_standard_number.carbon_emitted
                        temp_energy_number.save()
			
        types = ['Type A', 'Type B', 'Type C']
        resources = []
        tempresources = Resources.objects.all()
        for resource in tempresources:
                for t in types:
                        resources.append(resource.resource_name  + " " + t)
        return render(request, 'add_energy_numbers.html',{'resources':resources})


		
	
	

'''
	Views to manage session-related items.

	* sessions_list	  : List all sessions
	* new_session	  : Create a new session
	* user_id	  : Map users to ids
	* session_control : Control page for a session
	* budget_control  : View team budget (last submitted)

'''

from import_elements import *
from misc import Authenticate_Facilitator

def sessions_list(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	request.session['role'] = -1
	return render(request, 'sessions.html', {'sessions': Session.objects.all(), })

# Create new session:

def new_session(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	returnDict = {}
	returnDict.update(csrf(request))
	if request.method == 'GET':
		request.session['newsession_stage'] = 1
		return render(request, 'newsession.html', returnDict)
	elif request.method == 'POST':
		if request.session['newsession_stage'] == 1:
			# This section will run when the number of teams is posted
			teams = int(request.POST.get('teams'))
			# Generate IDs of new teams and pass them to the template
			last_team_id = 0
			try:
				last_team_id = Team.objects.latest('team_id')
				last_team_id = last_team_id.team_id
			except Team.DoesNotExist:
				last_team_id = -1
			returnDict['newTeams'] = [str(i) for i in range(last_team_id+1, last_team_id+teams+1)]
			returnDict['teamsEntered'] = teams
			request.session['new_teams'] = teams
			request.session['session_details'] = returnDict['sessionInfo'] = request.POST['session_details']
			# Set stage:
			request.session['newsession_stage'] = 2
			return render(request, 'newsession.html', returnDict)
		else:
			# This section will run when the session and teams themselves are submitted
			# Create session:
			last_session_id = 0
			try:
                                last_session_id = Session.objects.latest('session_id')
                                last_session_id = last_session_id.session_id
                        except Session.DoesNotExist:
                                last_session_id = -1
			
			newSession = Session(session_id=last_session_id+1,
						session_details=request.session['session_details'],
						no_of_teams=request.session['new_teams'] )

			# Create teams:
			last_team_id = 0
			try:
				last_team_id = Team.objects.latest('team_id')
				last_team_id = last_team_id.team_id
			except Team.DoesNotExist:
				last_team_id = -1
			newTeams = [Team(team_id=last_team_id+i+1, session=newSession) for i in range(0, request.session['new_teams'])]

			# Add default budget configurations for all of them:

			r = redis.StrictRedis(host='localhost', port=6379, db=0)
			for i in newTeams:
				for j in range(0, 18):
					r.lpush(i.team_id, 0)

			# Assign teams and roles to users:
			newSession.save()
			sessionState = SessionState(session=newSession, session_state=0)
			sessionState.save()
			for i in newTeams:
				i.save()
			for assignment in request.POST:
				if assignment == 'csrfmiddlewaretoken':
					continue
				# Each entry is of the format <role><team>: <user1> <user2> <user3>...
				theTeam = Team.objects.get(team_id=int(assignment[1:]))
				for theUser in request.POST[assignment].split():
					tempUserTeam = UserTeam(team = theTeam,
								user = User.objects.get(user_id=int(theUser)),
								role = Role.objects.get(role_id=int(assignment[0]))	)
					tempUserTeam.save()

			# Save session and teams AFTER users have been assigned stuff
			# Clean up:
			request.session.pop('session_details')
			request.session.pop('new_teams')
			
			return HttpResponse('Teams saved.')

	return HttpResponse('Invalid Request.')
				
			
# Get mapping of user_id - user:

def user_ids(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	theUsers = User.objects.exclude(user_name = 'facilitator')
	return render(request, 'userlist.html', {'users': theUsers, })

# Session Control Panel:

def session_control(request, session_id):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	request.session['session_id'] = session_id
	teams = Team.objects.filter(session_id = int(session_id))
	teamMembers = {}
	for team in teams:
		teamMembers[team.team_id] = [[], [], []]
		for userTeam in UserTeam.objects.filter( team_id=team.team_id ):
			teamMembers[team.team_id][userTeam.role_id].append(User.objects.get(user_id=userTeam.user_id).user_name)

	names = ''
	for i in teamMembers:
		for j in teamMembers[i]:
			for k in j:
				names = k + ' '

	states = ['12th FYP', 'Between 12th and 13th FYP', '13th FYP', 'Done']
	next_state_actions = ['End 12th FYP', 'Start 13th FYP', 'End 13th FYP', 'View Results']

	theState = SessionState.objects.get( session_id = int(session_id)).session_state
	state = states[theState]
	next_state_action = next_state_actions[theState]

	return render(request, 'session_control.html', {'teams': teamMembers,'sessionid': session_id, 'state': state,
								'next_state_action': next_state_action})

def budget_control(request, team_id):
        if not Authenticate_Facilitator(request):
                return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
        data = {}
        data['resources'] = Resources.objects.all()
        # Rearrange the data in the format [ [All Type A] [All Type B] [All Type C] ] and pass it to template
        temp = [[], [], []]
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        theBudget = [int(i) for i in r.lrange(team_id, 0, r.llen(team_id))]
        #tempBudget = r.lrange(team_id, 0, r.llen(team_id))
        for i in range(0, len(theBudget), 3):
                for j in range(0, len(temp)):
                        temp[j].append(theBudget[i+j])
        data['budgetConfig'] = temp
        data['team'] = "For Team "+str(team_id)
        return render(request, 'currentbudget.html', data)

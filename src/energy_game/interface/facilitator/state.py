'''
	Views to control the state of the game.

	* next_state	: Move game to the next state

'''
from import_elements import *
from misc import Authenticate_Facilitator

def next_state(request):
	if not Authenticate_Facilitator(request):
		return HttpResponse('Access Denied. You must be logged in as facilitator to view this page.')
	theStateObject = SessionState.objects.get( session_id = request.session['session_id'])
	session_obj = Session.objects.get( session_id = request.session['session_id'] )
	returnDict = {}
	returnDict.update(csrf(request))
	teams = Team.objects.filter(session_id = request.session['session_id'])
	if theStateObject.session_state == 0:
		# 12th FYP
		returnDict['plan'] = '12'
	elif theStateObject.session_state == 2:
		# 13th FYP
		returnDict['plan'] = '13'
	returnDict['teams'] = teams
	if request.method == 'GET':
		if theStateObject.session_state % 2 == 0:
			# End 12th or 13th FYP
			return render(request, 'end_plan.html', returnDict)
		elif theStateObject.session_state == 1:
			# Between 12th and 13th FYPs, wants to start 13th FYP
			theStateObject.session_state = theStateObject.session_state + 1
			if theStateObject.session_state == 4:
				return HttpResponse('Game already ended. We appreciate your enthusiasm, though!')
			# Log timestamp of state transition
			transitionObject = SessionStateTransition( session = session_obj,
								state = theStateObject.session_state )
			theStateObject.save()
			transitionObject.save()
			return HttpResponse('13th FYP started.')
		else:
			# Clearly in "done" state
			return HttpResponse('View results.')
	elif request.method == 'POST':
		theStateObject.session_state = theStateObject.session_state + 1
		if theStateObject.session_state == 4:
			return HttpResponse('Game already ended. We appreciate your enthusiasm, though!')
		# Log timestamp of state transition
		transitionObject = SessionStateTransition( session = session_obj,
							state = theStateObject.session_state )
		transitionObject.save()
		theStateObject.save()
		for team in teams:
			# Get messages for each role, and public
			for role in range(-1, 3):
				endMessage = EndMessage(state = theStateObject.session_state,
							message = request.POST[str(team.team_id)+'_'+str(role)],
							role = Role.objects.get(role_id=role),
							team = team)
				endMessage.save()
		return HttpResponse('FYP Finished. Messages Saved.')



'''
	Views to handle messages
	* quick_note_taker	: Send a new message
	* message_bank		: Select a message from the bank
'''

from import_elements import *
from interface.forms import QuickNote

def quick_note_taker(request, bank_id = -1):
	c = {'role': request.session['role'],
		'Roles': ['MOP', 'MNRE', 'DAE', 'Public'],
		'Teams': [ i.team_id for i in Team.objects.filter(session_id = request.session['session_id'])],
		}
	
	c.update(csrf(request))
	# Retreive default message from bank
	default_message = ""
	if bank_id != -1:
		default_message = (MessageBank.objects.get(message_id = bank_id)).contents
	c['default_text'] = default_message

	if request.method == 'POST':

		form = QuickNote(request.POST)
		if form.is_valid():
			# Check whether public or private message:
			if int(request.POST['role']) == 3:
				# Public message; create 3 notes
				for i in range(0, 3):
					newNote = Notes(note_title=request.POST['title'], note_content=request.POST['content'], user_id=request.session.get('userid'), role_id=i, team_id=int(request.POST.get('team')))
					newNote.save()
			else:
				# Private message
				newNote = Notes(note_title=request.POST['title'], note_content=request.POST['content'], user_id=request.session.get('userid'), role_id=request.POST.get('role'), team_id=int(request.POST.get('team')))
				newNote.save()
			return HttpResponse('Message sent.')
		else:
			return HttpResponse('Message failed to send.')

	return render_to_response('newmessage.html', c)


def message_bank(request):
	returnDict = {}
	messages = MessageBank.objects.all()
	returnDict['message_bank'] = messages[1:]
	return render_to_response('message_bank.html', returnDict)

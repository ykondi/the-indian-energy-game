# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class Session(models.Model):
    session_id = models.IntegerField(primary_key=True)
    session_details = models.TextField(blank=True)
    no_of_teams = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'session'

class SessionState(models.Model):
	session = models.ForeignKey(Session)
	session_state = models.IntegerField(default=0)
	class Meta:
		db_table = u'session_state'

class SessionStateTransition(models.Model):
	session = models.ForeignKey(Session)
	state = models.IntegerField()
    	timestamp = models.DateTimeField(auto_now_add=True)
	class Meta:
		db_table = u'session_state_transition'

class Team(models.Model):
    team_id = models.IntegerField(primary_key=True)
    session = models.ForeignKey(Session)
    class Meta:
        db_table = u'team'

class Role(models.Model):
    role_id = models.IntegerField(primary_key=True)
    instruction_set = models.TextField()
    budget = models.IntegerField()
    class Meta:
        db_table = u'role'

class User(models.Model):
    user_id = models.IntegerField(primary_key=True)
    user_name = models.CharField(max_length=135, blank=True)
    password = models.CharField(max_length=135, blank=True)
    class Meta:
        db_table = u'user'

class UserTeam(models.Model):
    team = models.ForeignKey(Team)
    user = models.ForeignKey(User)
    role = models.ForeignKey(Role)
    class Meta:
        unique_together = (("team", "user"),)
        db_table = u'user_team'

class Amount(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    team = models.ForeignKey(Team)
    number_1 = models.IntegerField(null=True, db_column=u'1', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_2 = models.IntegerField(null=True, db_column=u'2', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_3 = models.IntegerField(null=True, db_column=u'3', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_4 = models.IntegerField(null=True, db_column=u'4', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_5 = models.IntegerField(null=True, db_column=u'5', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_6 = models.IntegerField(null=True, db_column=u'6', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_7 = models.IntegerField(null=True, db_column=u'7', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_8 = models.IntegerField(null=True, db_column=u'8', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_9 = models.IntegerField(null=True, db_column=u'9', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_10 = models.IntegerField(null=True, db_column=u'10', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_11 = models.IntegerField(null=True, db_column=u'11', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_12 = models.IntegerField(null=True, db_column=u'12', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_13 = models.IntegerField(null=True, db_column=u'13', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_14 = models.IntegerField(null=True, db_column=u'14', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_15 = models.IntegerField(null=True, db_column=u'15', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_16 = models.IntegerField(null=True, db_column=u'16', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_17 = models.IntegerField(null=True, db_column=u'17', blank=True) # Field renamed because it wasn't a valid Python identifier.
    number_18 = models.IntegerField(null=True, db_column=u'18', blank=True) # Field renamed because it wasn't a valid Python identifier.
    class Meta:
        db_table = u'amount'
	ordering = ['timestamp']

class Notes(models.Model):
    note_id = models.AutoField(primary_key=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    note_title = models.CharField(max_length=135, blank=True)
    note_content = models.TextField(blank=True)
    team = models.ForeignKey(Team)
    role = models.ForeignKey(Role)
    user = models.ForeignKey(User)
    class Meta:
        db_table = u'notes'

class EndMessage(models.Model):
	state = models.IntegerField()
	message = models.TextField(blank=True)
	role = models.ForeignKey(Role)
	team = models.ForeignKey(Team)
	class Meta:
		db_table = u'end_message'

class MessageBank(models.Model):
    message_id = models.IntegerField(primary_key=True)
    contents = models.TextField(db_column='Contents') # Field name made lowercase.
    class Meta:
        db_table = u'message_bank'

class Messages(models.Model):
    message = models.ForeignKey(MessageBank, primary_key=True)
    note = models.ForeignKey(Notes)
    team = models.ForeignKey(Team)
    role = models.ForeignKey(Role)
    class Meta:
        db_table = u'messages'

class Resources(models.Model):
    resource_id = models.IntegerField(primary_key=True)
    resource_name = models.CharField(max_length=135)
    role = models.ForeignKey(Role)
    class Meta:
        db_table = u'resources'




class StandardResourcePrices(models.Model):
    resource_no = models.IntegerField(primary_key = True)
    cost_of_installation = models.IntegerField()
    cost_of_generation = models.IntegerField()
    carbon_emitted = models.IntegerField()
    class Meta:
        db_table = u'standard_resource_prices'

class ResourcePrice(models.Model):
    team = models.ForeignKey(Team)
    resource_no = models.ForeignKey(StandardResourcePrices)
    cost_of_installation = models.IntegerField()
    cost_of_generation = models.IntegerField()
    carbon_emitted = models.IntegerField()
    class Meta:
        db_table = u'resource_price'
        
class Constraints(models.Model):
   constraint_id = models.IntegerField( primary_key= True)
   session_id = models.ForeignKey(Session)
   lower_limit = models.IntegerField()
   upper_limit = models.IntegerField()
   class Meta:
	db_table = u'constraints'

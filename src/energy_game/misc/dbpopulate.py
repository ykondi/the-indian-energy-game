import sys
print 'attach "netmag.db" as db1;'
title = False
if len(sys.argv) != 3:
	print "Usage:", sys.argv[0], "<tsv file> <table name>"
	sys.exit()
columnString = 'insert into db1.' + sys.argv[2]
for i in open(sys.argv[1], 'r'):
	vals = i.split('\t')
	if title:
		columnString = 'insert into db1.' + sys.argv[2] + '('
		for j in vals:
			columString = columnString + j + ', '
		columnString[len(columnString)-2] = ')'
		title = not title
	else:
		# print 'insert into db1.User(user_id, user_name, password)'
		# print "values (0, "yashvanth", "ab1234")"
		print columnString + " values ", (vals[0], vals[1][:-2]), ";"

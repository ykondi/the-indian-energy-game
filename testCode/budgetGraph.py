from Tkinter import *
import threading
import time

resources = colour = w = None
bars = None
resourceInp = None
maxBudget = [ None, None]

def RefreshMaxBudget():
	global maxBudget
	global w
	global barCanvas
	w.delete( maxBudget[0] )
	barCanvas.delete( maxBudget[1] )
	maxBudget[0] = w.create_oval( 150, 150, 350, 350, dash = (4,4), outline = "gray", width = 3.0)
	maxBudget[1] = barCanvas.create_line( 320, 300, 450, 300, fill = "darkgray", width = 3.0)

def GetPartitions( values ):
	total = sum(values)
	if total == 0:
		total = 1
	chunks = [((float(i))/total) * 360 for i in values]
	return chunks, total

def SizeChanger():
	global resources
	global colour
	global resourceInp
	global w
	global resourceSum
	global bars
	global barCanvas
	resourceVal = [0, 0, 0]
	partitions = [120, 120, 120]
	borderColour = ["red", "green"]

	while 1:
		time.sleep(0.05)
		for i in range(0, 3):
			resourceVal[i] = resourceInp[i].get()

		partitions, diameter = GetPartitions( resourceVal )
		
		marker = 0
		for j in range(0, 3):
			w.coords( resources[j], (250-diameter/2, 250-diameter/2, 250+diameter/2, 250+diameter/2))
			w.itemconfig( resources[j], start = marker, extent = partitions[j]-1, outline = borderColour[diameter<200])
			marker = marker + partitions[j]

			barCanvas.coords( bars[j], (j*100, 500, j*100+90, 500-resourceVal[j]))
			resourceSum.set("Total : " + str(diameter))

		barCanvas.coords( bars[3], (320, 500, 450, 500-diameter))
		
		if diameter > 198:
			RefreshMaxBudget()


def WindowSetup():
	global budget
	global colour
	global resources
	global resourceInp
	global w
	global resourceSum
	global bars
	global barCanvas
	colour = ["yellow", "blue", "brown", "black"]
	resources = [None, None, None]
	bars = [None, None, None]
	resourceInp = []

	master = Tk()
	master.geometry("951x700")

	resourceSum = StringVar()
	resourceSum.set("Total : 0")

	w = Canvas(master, width=500, height=500, bg = "white", borderwidth = 1, relief = GROOVE)
	barCanvas = Canvas(master, width=450, height=500, bg = "white", borderwidth = 1, relief = RIDGE)
	w.place(x = 0, y = 0)
	barCanvas.place(x = 500, y = 0)

	maxBudget[0] = w.create_oval( 150, 150, 350, 350, dash = (4,4), outline = "darkgray", width = 5.0)
	maxBudget[1] = barCanvas.create_line( 320, 300, 450, 300, fill = "darkgray", width = 3.0)
	
	for i in range(0, len(resources)):
		resources[i] = w.create_arc( (250, 250, 250, 250), fill = colour[i], start = 120*i+1, extent = 120*(i+1), width = 4.0)
		bars[i] = barCanvas.create_rectangle( (i*110, 500, i*110+100, 490), fill = colour[i] )
	bars.append( barCanvas.create_rectangle( (3*110, 500, 3*110+100, 490), fill = colour[3] ) )

	for i in range(0, len(resources)):
		resourceInp.append( Scale( master, from_= 0, to = 250, orient = HORIZONTAL, bg = colour[i], length = 150) )
		(resourceInp[i]).pack(side = BOTTOM)

	sumTotal = Label( master, textvariable = resourceSum )
	sumTotal.pack(side = BOTTOM)

	graph1label = Label( master, text = "Graph 1")
	graph1label.place(x = 250, y = 50)
	graph2label = Label( master, text = "Graph 2")
	graph2label.place(x = 750, y = 50)

	sizeChangeThread = threading.Thread(target = SizeChanger)
	sizeChangeThread.daemon = True
	sizeChangeThread.start()

	w.mainloop()
	print "done with window"

if __name__ == "__main__":
	WindowSetup()

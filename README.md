Fields of View

The Indian Energy Game
======================

Documents
---------
The Documents folder is an archive of documentation related to the project.  

* newinterfaces.pdf:	Describes the first proposal for a new interface.
* graphQuestionaire:	List of questions for feedback form for budget graph.


testCode
--------
The Test Code folder contains any temporary scripts used to test specific
features of the application.

* budgetGraph.py:	Used to simulate the expanding pie chart graph proposed
		  	for visualizing the budget data.

prototype
---------
Sandbox for Django prototype

External Links
--------------
[Feedback form for Budget Graph](https://www.surveymonkey.com/s/CY5R5BL)  
[Wireframes](https://gomockingbird.com/mockingbird/#kntnwz2)

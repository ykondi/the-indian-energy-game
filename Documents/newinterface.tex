\documentclass{article}
\usepackage{fancyhdr}

\pagestyle{fancy}
\fancypagestyle{plain}{}
\fancyhf{}
\lhead{\small Fields of View: The Indian Energy Game}
\rhead{\thepage}

\begin{document}

\title{Interface Design Decisions}
\author{Anisha Nazareth \emph{\small and} \large Yashvanth K}
\maketitle

\begin{abstract}
This document covers the decisions made in
drafting the first version of the new interface for
the Indian Energy Game. While this interface
inherits quite a few attributes from the old one,
it also introduces a number of new elements that
could alter the experience of the players (and
possibly the facilitator, serendipitously).
\end{abstract}

\section{Note Recording Tool}
\paragraph{}
A large part of the Indian Energy Game (IEG)
is interpreting and acting on messages that
the facilitator passes to the players. This
was clearly illustrated in the decision tree
/ flow chart made for the players' perspective
of the game. In this regard, we feel that a
tool for the players to record their thoughts
and remarks on the current situation (ideally
after receiving a message) would be useful
to the player for quick reference to the
history of the game (ie. the sequence of
events that have led up to their current
state).\\

A few points are to be kept in mind here:
\subsection{Electronic Messages}
\paragraph{}
The physical medium still retains its position
as the primary mechanism for message delivery,
as messages are initially passed only physically.
After a short interval of time, the message will
be sent to the departments inbox. The player will 
recieve no notification that the message has 
arrived.
\paragraph{}
A mode of message delivery where the player
automatically gets access to messages as and 
when they are delivered was scrapped, 
as this may be (mis)used.
The player may ignore the physical messages
altogether, and treat this system as a purely
electronic one.
\paragraph{}
In addition to this, the recording of abstracted information
by the players may give an insight into how
exactly the player thinks through the game.
This may be useful for postgame analysis,
and to the facilitator during gameplay.
%\subsection{Discouraging Electronic Communication}
%\paragraph{}
%One potential method of misusing this tool
%that is specifically to be avoided is the
%players using it as an electronic mode of
%communication amongst themselves. With this
%in mind, a few measures have been taken to
%discourage its usage as an intra-team
%electronic message service.
%\begin{itemize}
%\item The interface for recording a note
%is to be made as heavy and formal as
%possible. The interface will be less like
%a Post-It viewing widget on a smartphone
%(a benchmark in terms of light, informal
%recording tools), and more like a forum-posting
%sort of complex textbox (with many options
%for formatting to lend it an important look).
%\item Notes can be edited only by the
%author. \footnote{Maybe even made read-only after
%posting. Subject to observations on user
%testing.}
%\end{itemize}
%If the players are still keen on leaving
%messages for each other (across departments),
%this mode of formal communication can be
%likened to passing memos, or issuing formal
%requests as members of the actual departments
%are bound to communicate in reality.\footnote{The
%level of urgency and willingness to communicate
%could be factors in a player deciding whether
%to interrupt his fellow teammates, or just leave
%a message for them to refer to/read at their
%own pace at a later time.}

\subsection{Tags}
\paragraph{}
Each note has certain tags associated with
it in order to make classification and ordering
easier (therefore allowing for quicker reference
by the user).
\begin{itemize}
\item \textbf{Implicit tags:} Attributes such as
the timestamp of the note
are implicit, and unchangeable by the
author.
\item \textbf{Explicit tags:} Optional
attributes that are specified by the author.
Potential usage: association between messages
and the note, note to self for future reference,
primary features of the strategy in use,
minutes of a team meeting,\footnote{These sort of
features enable support for longer, more involved
game sessions, facilitating deeper and more
complex gameplay.}
so on and so forth.
\end{itemize}

\subsection{Facilitator Communication Channel}
\paragraph{}
The channel for communication with the facilitator
has been integrated with the note-taking tool.
When the facilitator sends a message to the player,
it gets appended to the player's recorded notes
inbox.
\paragraph{Player Request:} When a player wants
to request new available information about a
particular topic. A form\footnote{A single line}
is filled out and sent to the faciltator. While
the content of the form itself is drawn from a
predefined structured language\footnote{This
provides potential for automation.}, the code can
be generated by clicking an icon or two\footnote{
Selecting which
category of information one requires}.
\paragraph{Facilitator Message:} When this case
would arise is unclear, but there is a provision
for the facilitator to add notes to the player's
inbox as and when it is required. This event would
colour the note-inbox icon specially, and alert
the player of the presence of a new message.

\subsection{Interface}
\subsubsection{Adding New Notes}
\paragraph{}
A small icon on one corner of the page will make
available a minimalistic note-recording tool in
a pop-up window.
%\paragraph{}
A field to enter text, some basic formatting options,
a button to discard the note, and a button to save
the note are provided.
\subsubsection{Viewing Recorded Notes}
A larger button signifying the full-scale note
managing tool resides next to the "add new note"
button. Clicking this opens up the recorded notes
(in an email-inbox sort of form) in a new tab,
with an option to add a new note here as well.
This view also contains the form to request new
electronic messages/information from the
facilitator.

\subsection*{}
Hence, the note recording system and its
motivations are introduced. It should be
noted that its primary intention was to serve
as a direct, neutral mode of archiving
both abstracted and raw information.

\section{Player Necessities}
In order to be able to play the game effectively the player must have the following:
\begin{itemize}
\item access to the energy data for his department
\item an input mechanism to to enter figures in order to 
reach the games objectives
\item a mechanism to calculate and display how close he is
to achieving each objective
\end{itemize}

\subsection{Accessing the energy data}
\paragraph{}A physical copy of the energy data for each department is provided to the players
at the start of the game. However if the physical copy isn't available to a player for immediate reference
(it might have been temporarily misplaced for instance), a digital copy would be useful.
\paragraph{} In order to address this, the interface has a small button present at the top left hand corner of the screen.
Upon clicking the button, a pop up window containing the energy figures appears. This idea was 
favoured over opening up a new tab with the figures, as it removes the need for the players to keep switching tabs.
The button does not occupy much space and the data would be made available
upon a simple click.
\paragraph{}An initial idea was to display the energy figures on the screen directly. However this would not only take up a lot of space
but would also be fairly redundant. The digital record of these figures is being provided merely
as a supplement to the physical copy. Hence players would not need to constantly view these figures.
\paragraph{}An alternative to this was to have the player scroll up or down to view these figures.
However this can get tedious, especially when in a hurry. 

\subsection{Input Mechanism}
\paragraph{}The interfaces primary mode for taking input is through text boxes. Each text box has a `+' and `-'
button attached to it. If the user is not comfortable working with numbers/raw data, these buttons can be used
to increment or decrement the amount of any resource present based on inferences drawn from how
the overall derived values (cost/KWh, budget usage, etc.) are changed.\footnote{See next section: \emph{Mapping Progress}}
The values in the field
increment or decrement at 1 unit/ms for as long as the button is pressed.\footnote{Subject to actual testing}
\paragraph{} The text boxes for each resource are not directly present on the screen. Instead, the total amount of power generated by each resource, as well as the break up of this power into various types is displayed. The text box for each type becomes available upon clicking on that type.
\paragraph{Discarded Ideas:}
An alternative to these buttons would have been to use sliders, but this idea was dropped as sliders are less precise.
The idea of using icons which can be clicked and dropped was also discarded, as in addition to trivializing the
game, discretizing the values for blocks of input would be difficult.
\paragraph{} The money allocated for R\&D and O\&M is also taken in in the same way. The text boxes for filling in R\&D and O\&M become available upon clicking on the R\&D and O\&M buttons respectively.

\section{Mapping Progress}
The department's progress is mapped using a combination of graphs and numbers.
\subsection{Numbers}
The numbers are presented on the screen in the same way as in the current interface; They are present in a row at the top of the screen.
\subsection{Graphs}
\paragraph{}
Graphs were added so that the current state of the game (standing with respect to the given objectives)
may be understood despite inexperience in mentally processing raw data.
\paragraph{}
There are essentially four graphs:
\begin{itemize}
\item \textbf{1. and 2.} monitor the price/KWh with respect to the target specified in the objectives, for that
department and the overall team respectively.
\item \textbf{3.} monitors $CO_{2}$ emissions with respect to the maximum permissible amount as specified in the objectives.
\item \textbf{4.} monitors the amount of money spent as a chunk of the budget allocated for that department.
\end{itemize}
A more detailed description:
\begin{enumerate}
\item The first graph monitors the price/KWh of the department
in question over time. The graph has an `ideal' line running through it at
 3 Rs/KWh. A set of horizontal bar graphs represents the contribution of each
resource within a department to the overall price/KWh for that department.
For each resource the quantity mapped is:
\[\frac{price}{KWh}_{resource} \times No.\ of \ MW \ generated \ by the resource as a percentage of the whole\]
In addition, for each resource a second bar is present which plots the No. of MW generated by that resource as a percentage of the whole.

\item The second graph monitors the price/KWh for the team as a whole over time.
A set of bar graphs represent the contribution of each department to the overall
price/KWh value. Quantity graphed:
\[\frac{price}{KWh}_{department} \times No.\ of\ MW\ generated\ by\ dept.\ as\ percentage\ of\ whole\]
In addition, for each department a second bar is present which plots the No. of MW generated by that department as a percentage of the whole.

\item The third graph monitors the amount of money used by that department at that instant. This graph is essentially a pie chart with an expanding or 
contracting circle. An ideal circle is marked out on the graph and denotes the budget of the department.
If the department is overshooting the budget their circle will be larger than the ideal circle and will be red, otherwise it will
be smaller (or the same size) and will be green. The amount of money used by each resource is depicted as a slice of the pie
in that resource's name.

\item The fourth graph monitors the $CO_{2}$ emissions of the team over time.
The current breakup of the team's $CO_{2}$ emissions per department is
depicted in a stacked bar graph.
There is an ideal line on the graph which correseponds
to the limit set. If the team's stack exceeds the set limit, it is marked in
red. Otherwise, in green.

\end{enumerate}

The user can view any two graphs at a time. A graph button is present at the
topright hand corner of the screen. Upon clicking the button a drop down with 
pictorial representations and a short description of each graph appears. The user can
select a maximum of two graphs to view, based on his preferences and priorities.

\section{Additional Features}
\subsection{Unquantifiables}
The unquantifiable factors governing the playing
of the game are modelled in the player-facilitator
communication channel mentioned in the note-taking
tool section\footnote{See Section 1.3}. The tool
allows the facilitator to provide information that
may turn out to be relevant to the player without
explicitly saying so. The grouping of messages
from the facilitator in one place (the recorded
notes inbox) sorted by their respective tags
provides the player with all the available information
at hand regarding an issue he/she may want to
gauge.

\subsection{Minor Additions}
\paragraph{}
There are a number of minor additions present. For example, the percentage of the budget allocated to R\&D and O\&M is displayed along with the R\&D and O\&M text boxes.
The amount of power generated and the target amount of power to be generated is also diplayed. In addition to this, the contribution of each resource  
to the overall power distribution and the contribution of that resource to the
previous year's power distribution can be accessed by clicking a button.

\section*{}
The new interface was designed with a view to be
inclusive of and usable for a larger audience. It
aims to facilitate more intuitive interpretation
of the game state (with graphs and such) and
help the user make more informed decisions
(contributed to by easy access to the history of the
current game). The interface itself is intended
to be more meaningful to the player, but caution
was exercised in its design so that the game
mechanics themselves remain intact.\\ \\
Thus, the new interface is introduced.

\end{document}
